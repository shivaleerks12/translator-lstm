# Translator- LSTM

Built a Translator using LSTM for a programming assignment in Deep Learning Course

The Repository contains:
1. Questions pdf file which has explanation to the problem statement and links to dataset
2. Q1_code and Q2_code .ipynb files which contain the code of the questions
3. Report pdf containing explanation of code and and visuals of result obtained
